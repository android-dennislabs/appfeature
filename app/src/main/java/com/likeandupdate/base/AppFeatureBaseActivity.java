package com.likeandupdate.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.appfeature.interfaces.VersionCallback;
import com.appfeature.utility.UIModel;
import com.likeandupdate.AppApplication;

public abstract class AppFeatureBaseActivity extends AppCompatActivity implements VersionCallback {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppApplication.getInstance().initAppFeature(this);
    }

    @Override
    public void showVersionDialog(Boolean restrictToUpdate, UIModel uiData) {
        onVersionUpdate();
    }

    protected abstract void onVersionUpdate();

}