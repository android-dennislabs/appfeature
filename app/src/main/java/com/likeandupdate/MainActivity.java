package com.likeandupdate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.interfaces.VersionCallback;
import com.appfeature.utility.RemoteModel;
import com.appfeature.utility.ReviewApi;
import com.helper.util.DayNightPreference;
import com.likeandupdate.base.AppFeatureBaseActivity;


public class MainActivity extends AppFeatureBaseActivity implements VersionCallback, RemoteCallback {

    @Override
    public void onComplete(String status, RemoteModel remoteData) {

        AppApplication.getInstance().initAppFeature(this );
        AppApplication.getInstance().getAppsFeature(this).removeRemoteCallback(hashCode());
        new ReviewApi(this).requestReviewFlowAPI();
    }

    @Override
    public void onError(String message) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SwitchCompat switchCompat = findViewById(R.id.switchCompat);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
            switchCompat.setChecked(true);

        switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> DayNightPreference.setNightMode(MainActivity.this , isChecked));
    }

    @Override
    protected void onVersionUpdate() {

    }


    public void onRatingClick(View view) {
        startActivity(new Intent(MainActivity.this, ActivityRating.class));
    }

}
