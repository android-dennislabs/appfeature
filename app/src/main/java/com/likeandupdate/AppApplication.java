package com.likeandupdate;

import android.app.Activity;
import android.widget.RelativeLayout;

import com.appfeature.AppsFeature;
import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.interfaces.VersionCallback;
import com.helper.application.ActivityLifecycleObserver;
import com.helper.application.BaseApplication;

public class AppApplication extends BaseApplication {

    private static AppApplication appApplication;


    public static AppApplication getInstance() {
        return appApplication;
    }

    @Override
    public boolean isDebugMode() {
        return BuildConfig.DEBUG;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appApplication = this;
        ActivityLifecycleObserver.getInstance().register(this);
    }

    @Override
    public void initLibs() {

    }

    private AppsFeature appsFeature;

    public void initAppFeature(Activity activity) {
        if (appsFeature == null && activity != null ) {
            appsFeature = AppsFeature.getInstance(activity, BuildConfig.VERSION_CODE)
                    .setShowInLayoutRatingCard(false);
            if (activity instanceof VersionCallback) {
                appsFeature.addVersionCallback((VersionCallback) activity);
            }
            if (activity instanceof RemoteCallback) {
                appsFeature.addRemoteCallback( activity.hashCode() , (RemoteCallback) activity);
            }
            appsFeature.init();
        }
    }

    public AppsFeature getAppsFeature(Activity activity) {
        if(appsFeature == null){
            initAppFeature(activity);
        }
        return appsFeature;
    }

    public void showRating(RelativeLayout layout){
        if(appsFeature!=null){
            appsFeature.showRating(layout);
        }
    }

}
