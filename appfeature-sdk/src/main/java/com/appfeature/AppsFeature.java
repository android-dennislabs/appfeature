package com.appfeature;

import android.app.Activity;
import android.util.Log;
import android.widget.RelativeLayout;

import com.appfeature.feature.RateUs;
import com.appfeature.feature.VersionUpdate;
import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.interfaces.VersionCallback;
import com.appfeature.ui.InAppUpdateManagerKTX;
import com.appfeature.utility.Rating;
import com.appfeature.utility.RemoteConfig;
import com.appfeature.utility.RemoteModel;
import com.appfeature.utility.UserPreference;
import com.appfeature.utility.Utility;
import com.appfeature.utility.Version;
import com.helper.application.ActivityLifecycleObserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Abhijit Rao
 * @version 1.0
 * @since 2018.12.07
 */
public class AppsFeature {

    private static AppsFeature _instance;
    private final Activity activity;
    private int appVersionCode;
    private String mRemoteConfigStatus;
    private RemoteModel mRemoteData;
    private UserPreference userPref;
    private boolean isRemoteSync = false;
    //    private List<RemoteCallback> remoteCallbackList = new ArrayList<>();
    private List<VersionCallback> versionCallback = new ArrayList<>();
    private HashMap<Integer, RemoteCallback> remoteCallbackList = new HashMap<>();

    private boolean isShowInLayoutRatingCard = true;
    private boolean isShowRatingDialog = true;

    public boolean isRemoteSync() {
        return isRemoteSync;
    }

    private AppsFeature(Activity activity, int appVersionCode) {
        this.activity = activity;
        this.userPref = new UserPreference(activity, activity.getApplicationContext().getPackageName());
        this.appVersionCode = appVersionCode;
    }

    public static AppsFeature getInstance(Activity activity, int appVersionCode) {
        if (_instance == null) {
            _instance = new AppsFeature(activity, appVersionCode);
        }
        return _instance;
    }

    public static AppsFeature get() {
        return _instance;
    }

    public Activity getActivity() {
        return activity;
    }

    //    public AppsFeature addRemoteCallback(RemoteCallback remoteCallback) {
//        if (remoteCallbackList != null && remoteCallback != null) {
//            remoteCallbackList.add(remoteCallback);
//        }
//        return this;
//    }

    public AppsFeature addVersionCallback(VersionCallback versionCallback) {
        this.versionCallback.add(versionCallback);
        return this;
    }

    public AppsFeature addRemoteCallback(int hashCode, RemoteCallback remoteCallback) {
        try {
            this.remoteCallbackList.put(hashCode, remoteCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public void removeRemoteCallback(int hashCode) {
        try {
            if (remoteCallbackList != null && remoteCallbackList.size() > 0 && remoteCallbackList.get(hashCode) != null) {
                remoteCallbackList.remove(hashCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AppsFeature init() {
        Log.d("appUpdateManager ", " AppsFeature init()");
        RemoteConfig.newInstance(activity).fetchData(hashCode(), new RemoteCallback(){
            @Override
            public void onComplete(String status, RemoteModel remoteData) {
                Log.d("appUpdateManager ", " AppsFeature complete");
                Utility.log(status);
                isRemoteSync = true;
                mRemoteConfigStatus = status;
                mRemoteData = remoteData;
                initRatingProcess();
                if (versionCallback != null) {
                    initVersionProcess();
                }
//                        updateCallback(status, remoteData);
                refreshRemoteCallback();
            }

            @Override
            public void onError(String message) {
                Log.e("appUpdateManager ", "onError : " + message);
                Utility.log(message);
            }
        });
        if (ActivityLifecycleObserver.getInstance().getCurrentActivity() == null && activity != null) {
            Utility.toast(activity, "AppFeature integration error : call ActivityLifecycleObserver.getInstance().register(this); in your Application class");
            Utility.logIntegration("AppFeature", "AppFeature integration error : call ActivityLifecycleObserver.getInstance().register(this); in your Application class");
        }
        return this;
    }

    private void refreshRemoteCallback() {
        try {
            if (remoteCallbackList != null && remoteCallbackList.size() > 0) {
                for (Integer integer : remoteCallbackList.keySet()) {
                    if (remoteCallbackList.get(integer) != null) {
                        remoteCallbackList.get(integer).onComplete("true", null);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void updateCallback(String status, RemoteModel remoteData) {
//        if (remoteCallbackList != null && remoteCallbackList.size() > 0) {
//            for (RemoteCallback remoteCallback : remoteCallbackList) {
//                if (remoteCallback != null) {
//                    remoteCallback.onComplete(null, null);
//                }
//            }
//        }
//    }

 /*   public void initVersionProcessAuto(Activity context) {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(context);

// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

            }
        });
    }
*/
    public void initVersionProcess() {
        Log.d("appUpdateManager ", " initVersionProcess");
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :102 (Invalid Request)");
            return;
        }
        Version version = mRemoteData.getVersion();
        VersionUpdate.newInstance(appVersionCode, version)
                .setNotificationType(version.getNotificationType())
                .setListener((restrictToUpdate, uiData) -> {
//                        VersionDialog.newInstance(activity, restrictToUpdate, uiData)
//                                .show();
                    Log.d("appUpdateManager ", " initVersionProcess listen");
                    new InAppUpdateManagerKTX(!restrictToUpdate , activity);
                    if (versionCallback != null) {
                        for (VersionCallback callback : versionCallback) {
                            callback.showVersionDialog(restrictToUpdate, uiData);
                        }
                    }
                })
                .init();
    }

    public void initRatingProcess() {
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :103 (Invalid Request)");
            return;
        }
        Rating rating = mRemoteData.getRating();
        if (validateVisibility()) {
            RateUs.newInstance(activity)
                    .setSessionFirstInstallTime(rating.getSessionFirstInstallTime())
                    .setSessionRepeatTime(rating.getSessionRepeatTime())
                    .setSessionDifferenceTime(rating.getSessionDifferenceTime())
                    .setRatingShowOnlyOnce(rating.isRatingShowOnlyOnce())
                    .setRatingApi(rating.isRatingApi())
                    .setNegativeButtonPressedAndNotShowRating(rating.isNegativeButtonPressedAndNotShowRating())
                    .setUiData(rating.getUiModel())
                    .init();
        }
    }

    /**
     * @param relativeLayout : adding rating card view on this ViewGroup
     */
    public void showRating(RelativeLayout relativeLayout) {
        if (isShowInLayoutRatingCard && RateUs.getInstance() != null && relativeLayout != null) {
            RateUs.getInstance().showUI(relativeLayout);
        }
    }

    /**
     * Show Rating by In-App Review API
     */
    public void showRatingDialog() {
        if (isShowRatingDialog && RateUs.getInstance() != null) {
            RateUs.getInstance().showUI(null);
        }
    }

    private boolean validateVisibility() {
//        boolean isOnce = userPref.isRatingShowOnlyOnce();
//        boolean neverShow = userPref.isRatingNegativeButtonPressedAndNotShow();
        if (userPref.isRatingSubmitted()) {
            return false;
        }
        return true;
    }

    public boolean isShowInLayoutRatingCard() {
        return isShowInLayoutRatingCard;
    }

    public AppsFeature setShowInLayoutRatingCard(boolean showInLayoutRatingCard) {
        isShowInLayoutRatingCard = showInLayoutRatingCard;
        return this;
    }

    public boolean isShowRatingDialog() {
        return isShowRatingDialog;
    }

    public AppsFeature setShowRatingDialog(boolean showRatingDialog) {
        isShowRatingDialog = showRatingDialog;
        return this;
    }
}
