package com.appfeature.ui;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.appfeature.R;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.helper.application.ActivityLifecycleObserver;

public class InAppUpdateManager {

    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE = 124;
    private boolean updateTypeFlexible;
    private int updateType;
    private Activity activity ;

    private Activity getActivity(){
        if (ActivityLifecycleObserver.getInstance() !=null && ActivityLifecycleObserver.getInstance().getCurrentActivity() != null ){
            activity = ActivityLifecycleObserver.getInstance().getCurrentActivity();
        }
        return activity ;
    }

    public InAppUpdateManager(boolean updateTypeFlexible , Activity activity) {
        this.updateTypeFlexible = updateTypeFlexible;
        updateType = updateTypeFlexible ? AppUpdateType.FLEXIBLE : AppUpdateType.IMMEDIATE;
        if ( activity != null ) {
            appUpdateManager = AppUpdateManagerFactory.create(activity);
            checkUpdate();
        }else {
            Log.e("appUpdateManager ", " activity == null ");
        }
    }

    private void checkUpdate() {
        Log.e("appUpdateManager ", " checkUpdate");
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(updateType)) {
                Log.e("appUpdateManager ", " UPDATE_AVAILABLE");
                startUpdateFlow(appUpdateInfo);
                if (updateTypeFlexible)
                    addListener();
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                Log.e("appUpdateManager ", " DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS");
                if (updateTypeFlexible)
                    popupSnackbarForCompleteUpdate();
                else
                    startUpdateFlow(appUpdateInfo);

            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                Log.e("appUpdateManager ", " DOWNLOADED");
                if (updateTypeFlexible)
                    popupSnackbarForCompleteUpdate();
            }
        });
    }

    InstallStateUpdatedListener listener = state -> {
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            // After the update is downloaded, show a notification
            // and request user confirmation to restart the app.
            popupSnackbarForCompleteUpdate();
        }
    };

    private void addListener() {
        appUpdateManager.registerListener(listener);
    }

    private void popupSnackbarForCompleteUpdate() {
        Log.e("appUpdateManager ", " popupSnackbarForCompleteUpdate");
        try {
            Activity activityCurrent = getActivity();
            if ( activityCurrent != null ) {
                View view = activityCurrent.findViewById(android.R.id.content);
                if ( view != null ) {
                    try {
                        Log.e("appUpdateManager ", " show snackbar");
                        Snackbar mySnackbar = Snackbar.make(view,
                                "Update Pending", Snackbar.LENGTH_LONG);
//                                activityCurrent.getString(R.string.af_update_msg), Snackbar.LENGTH_LONG);
                        mySnackbar.setAction("Install", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e("appUpdateManager ", " Update started");
                                appUpdateManager.completeUpdate();
                            }
                        });
                        mySnackbar.setTextColor(ContextCompat.getColor(activityCurrent , R.color.af_whiteColor));
                        mySnackbar.setActionTextColor(ContextCompat.getColor(activityCurrent , R.color.af_colorAccent));
//                        mySnackbar.getView().setBackgroundColor(ContextCompat.getColor(activityCurrent , R.color.af_colorPrimary));
                        mySnackbar.show();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        showDialog(activityCurrent , "Update Pending");
//                        showDialog(activityCurrent , activityCurrent.getString(R.string.af_update_msg));
                    }
                } else {
                    showDialog(activityCurrent , "Update Pending");
//                    showDialog(activityCurrent , activityCurrent.getString(R.string.af_update_msg));
                }
            }else {
                Log.e("appUpdateManager ", " activity == null ");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void showDialog(Context context, String title) {
        try {
            BottomSheetDialog dialog = new BottomSheetDialog (context);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_snack_bar);
            TextView tvTitle = dialog.findViewById(R.id.tv_dialog_title);
            View btnDialog = dialog.findViewById(R.id.btn_dialog);
            if (tvTitle != null) {
                tvTitle.setText(title);
            }
            if (btnDialog != null) {
                btnDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("appUpdateManager ", " Update started");
                        appUpdateManager.completeUpdate();
                        dialog.cancel();
                    }
                });
            }
            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            Activity activityCurrent = getActivity();
            if ( activityCurrent != null ) {
                appUpdateManager.startUpdateFlowForResult(appUpdateInfo, updateType, activityCurrent, IMMEDIATE_APP_UPDATE_REQ_CODE);
            }else {
                Log.e("appUpdateManager ", " activity == null ");
            }
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }
}
