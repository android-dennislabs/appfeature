package com.appfeature.ui

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.appfeature.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.ktx.requestAppUpdateInfo
import com.helper.application.ActivityLifecycleObserver
import kotlinx.coroutines.launch

class InAppUpdateManagerKTX(
    private val updateTypeFlexible: Boolean,
    private var activity: Activity
) {

    companion object {
        private const val IMMEDIATE_APP_UPDATE_REQ_CODE = 124
        private const val TAG = "InAppUpdateManager"
    }

    private val appUpdateManager = AppUpdateManagerFactory.create(activity)
    private val updateType = if (updateTypeFlexible) AppUpdateType.FLEXIBLE else AppUpdateType.IMMEDIATE

    private val currentActivity: Activity?
        get() = ActivityLifecycleObserver.getInstance()?.currentActivity ?: activity

    init {
        currentActivity?.let {
            checkUpdate()
        }
    }

    private fun checkUpdate() {
        Log.d(TAG, "checkUpdate")
        if (currentActivity is LifecycleOwner) {
            (currentActivity as LifecycleOwner).lifecycleScope.launch {
                try {
                    val appUpdateInfo = appUpdateManager.requestAppUpdateInfo()
                    when {
                        appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                                && appUpdateInfo.isUpdateTypeAllowed(updateType) -> {
                            Log.d(TAG, "UPDATE_AVAILABLE")
                            startUpdateFlow(appUpdateInfo)
                            if (updateTypeFlexible) addListener()
                        }
                        appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS -> {
                            Log.d(TAG, "DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS")
                            if (updateTypeFlexible) popupSnackbarForCompleteUpdate()
                            else startUpdateFlow(appUpdateInfo)
                        }
                        appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED -> {
                            Log.d(TAG, "DOWNLOADED")
                            if (updateTypeFlexible) popupSnackbarForCompleteUpdate()
                        }
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "Error checking for updates", e)
                }
            }
        } else {
            Log.e(TAG, "Activity is not a LifecycleOwner")
        }
    }

    private val listener = InstallStateUpdatedListener { state ->
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            popupSnackbarForCompleteUpdate()
        }
    }

    private fun addListener() {
        appUpdateManager.registerListener(listener)
    }

    private fun popupSnackbarForCompleteUpdate() {
        Log.d(TAG, "popupSnackbarForCompleteUpdate")
        try {
            val activityCurrent = currentActivity
            activityCurrent?.findViewById<View>(android.R.id.content)?.let { view ->
                try {
                    Log.d(TAG, "show snackbar")
                    Snackbar.make(view, "Update Pending", Snackbar.LENGTH_LONG)
                        .setAction("Install") {
                            Log.d(TAG, "Update started")
                            appUpdateManager.completeUpdate()
                        }
                        .setTextColor(ContextCompat.getColor(activityCurrent, R.color.af_whiteColor))
                        .setActionTextColor(ContextCompat.getColor(activityCurrent, R.color.af_colorAccent))
                        .show()
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    showDialog(activityCurrent, "Update Pending")
                }
            } ?: activityCurrent?.let { showDialog(it, "Update Pending") }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun showDialog(context: Context, title: String) {
        try {
            BottomSheetDialog(context).apply {
                setCancelable(true)
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setContentView(R.layout.dialog_snack_bar)
                findViewById<TextView>(R.id.tv_dialog_title)?.text = title
                findViewById<View>(R.id.btn_dialog)?.setOnClickListener {
                    Log.d(TAG, "Update started")
                    appUpdateManager.completeUpdate()
                    cancel()
                }
                show()
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    private fun startUpdateFlow(appUpdateInfo: AppUpdateInfo) {
        try {
            currentActivity?.let { activity ->
                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    activity,
                    AppUpdateOptions.defaultOptions(updateType),
                    IMMEDIATE_APP_UPDATE_REQ_CODE
                )
            } ?: Log.e(TAG, "activity == null")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}