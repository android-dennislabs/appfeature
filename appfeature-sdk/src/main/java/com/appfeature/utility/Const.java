package com.appfeature.utility;

class Const {
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    static final String DEFAULT_DATE_FORMAT = "yyyyMMddHH";
}
