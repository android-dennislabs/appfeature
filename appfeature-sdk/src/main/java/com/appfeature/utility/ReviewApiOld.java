//package com.appfeature.utility;
//
//import android.app.Activity;
//import android.text.TextUtils;
//
//import androidx.annotation.NonNull;
//
//import com.appfeature.analytics.AppFeatureAnalytics;
//import com.google.android.play.core.review.ReviewInfo;
//import com.google.android.play.core.review.ReviewManager;
//import com.google.android.play.core.review.ReviewManagerFactory;
//import com.google.android.play.core.tasks.OnCompleteListener;
//import com.google.android.play.core.tasks.OnFailureListener;
//import com.google.android.play.core.tasks.OnSuccessListener;
//import com.google.android.play.core.tasks.Task;
//
//public class ReviewApi {
//
//    private final Activity activity;
//    private final ReviewManager manager;
//    private final AppFeatureAnalytics analyticsUtil;
//
//    public ReviewApi(Activity activity) {
//        this.activity = activity;
//        this.manager = ReviewManagerFactory.create(activity);
//        this.analyticsUtil = AppFeatureAnalytics.getInstance(activity);
//    }
//
//    private interface ReviewAPIStatus {
//        void onComplete(String status);
//    }
//
//    public void requestReviewFlowAPI() {
//        requestReviewFlowAPI(null);
//    }
//
//    public void requestReviewFlowAPI(final ReviewAPIStatus callback) {
//        if (manager != null) {
//            Task<ReviewInfo> request = manager.requestReviewFlow();
//            request.addOnCompleteListener(new OnCompleteListener<ReviewInfo>() {
//                @Override
//                public void onComplete(@NonNull Task<ReviewInfo> task) {
//                    if (task.isSuccessful()) {
//                        // We can get the ReviewInfo object
//                        ReviewInfo reviewInfo = task.getResult();
//                        manager.launchReviewFlow(activity, reviewInfo)
//                                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                                    @Override
//                                    public void onSuccess(Void result) {
//                                        logWarning(callback, "In-app review launchReviewFlow onSuccess finished");
//                                    }
//                                })
//                                .addOnFailureListener(new OnFailureListener() {
//                                    @Override
//                                    public void onFailure(Exception e) {
//                                        logWarning(callback, "In-app review launchReviewFlow onFailure, reason=" + e.getMessage());
//                                    }
//                                });
//                        if (analyticsUtil != null) {
//                            analyticsUtil.onLaunchInAppReview("true");
//                        }
//                    } else {
//                        if (task.getException() != null) {
//                            logWarning(callback, "In-app review requestReviewFlow task.isSuccessful() request failed, reason=" + task.getException().toString());
//                        }
//                    }
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(Exception e) {
//                    logWarning(callback, "In-App requestReviewFlow onFailure, reason=" + e.getMessage());
//                }
//            });
//        }
//    }
//
//    private void logWarning(ReviewAPIStatus callback, String message) {
//        if (TextUtils.isEmpty(message))
//            return;
//        Utility.log(message);
//        if (callback != null) {
//            callback.onComplete(message);
//        }
//    }
//
//}
