package com.appfeature.utility;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.appfeature.BuildConfig;
import com.appfeature.R;
import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.ui.RemoteConfigManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;


public class RemoteConfig {

    private static final String KEY_APPSFEATURE = "_appsfeature";
    private final String packageName;

    private RemoteConfig(Activity activity) {
        this.packageName = activity.getApplicationContext().getPackageName();
    }

    public static RemoteConfig newInstance(Activity activity) {
        return new RemoteConfig(activity);
    }

    public void fetchData(int hashCode, RemoteCallback remoteCallback) {
        RemoteConfigManager.getInstance().fetch(hashCode, new RemoteConfigManager.RemoteCallback() {
            @Override
            public void onComplete(String status) {
                Log.d("appUpdateManager ", " fetchData:onComplete("+status+")");
                String configKey = getAppsfeatureKey();
                displayWelcomeMessage(status, RemoteConfigManager.getInstance().getJson(configKey), remoteCallback);
            }
        });
    }

    private void displayWelcomeMessage(String status, String ratingAndUpdate, RemoteCallback remoteCallback) {
        Log.d("appUpdateManager ", "ratingAndUpdate : " + ratingAndUpdate);
        if(TextUtils.isEmpty(ratingAndUpdate)){
            remoteCallback.onError("Firebase config is not setup yet!");
            return;
        }
        try {
            JSONObject ratingAndUpdateObj = new JSONObject(ratingAndUpdate);
            JSONObject ratingObj = ratingAndUpdateObj.getJSONObject("rating");
            JSONObject updateObj = ratingAndUpdateObj.getJSONObject("app_update");
            Rating rating = Utility.parseRatingJsonData(ratingObj);

            Version version = Utility.parseUpdateJsonData(updateObj);

            remoteCallback.onComplete(status, new RemoteModel(version,rating));
        } catch (JSONException e) {
            e.printStackTrace();
            remoteCallback.onError(e.toString());
        }
    }

    private String getAppsfeatureKey() {
        return packageName.replaceAll("\\.","_")+KEY_APPSFEATURE;
    }
}
