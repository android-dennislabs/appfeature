package com.appfeature.utility

import android.app.Activity
import com.appfeature.analytics.AppFeatureAnalytics
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory

class ReviewApi(private val activity: Activity) {

    private val manager: ReviewManager = ReviewManagerFactory.create(activity)
    private val analyticsUtil: AppFeatureAnalytics = AppFeatureAnalytics.getInstance(activity)

    fun interface ReviewAPIStatus {
        fun onComplete(status: String)
    }

    fun requestReviewFlowAPI() {
        requestReviewFlowAPI(null)
    }

    fun requestReviewFlowAPI(callback: ReviewAPIStatus? = null) {
        manager.requestReviewFlow().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val reviewInfo = task.result
                manager.launchReviewFlow(activity, reviewInfo).addOnCompleteListener { flowTask ->
                    if (flowTask.isSuccessful) {
                        logWarning(callback, "In-app review launchReviewFlow completed successfully")
                    } else {
                        logWarning(callback, "In-app review launchReviewFlow failed to complete")
                    }
                }
                analyticsUtil.onLaunchInAppReview("true")
            } else {
                task.exception?.let { exception ->
                    logWarning(callback, "In-app review requestReviewFlow task.isSuccessful() request failed, reason=${exception}")
                }
            }
        }
    }

    private fun logWarning(callback: ReviewAPIStatus?, message: String) {
        if (message.isNotEmpty()) {
            Utility.log(message)
            callback?.onComplete(message)
        }
    }
}