package com.appfeature.utility;

public class RemoteModel {
    private Version version;
    private Rating rating;

    public RemoteModel(Version version, Rating rating) {
        this.version = version;
        this.rating = rating;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
}
