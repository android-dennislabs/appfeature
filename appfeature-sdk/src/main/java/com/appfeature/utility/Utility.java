package com.appfeature.utility;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class Utility {
    public static final String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=";

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }
    public static double parseDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public static void log(String message){
//		Log.d("@-Test", message);
    }
    public static void toast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isValid(String mRemoteConfigStatus) {
        return !TextUtils.isEmpty(mRemoteConfigStatus)&& mRemoteConfigStatus.equals(Const.SUCCESS);
    }

    static Rating parseRatingJsonData(JSONObject ratingObj) throws JSONException {
        Rating rating = new Rating();
        JSONObject uiObj = ratingObj.getJSONObject("ui");
        JSONObject logicObj = ratingObj.getJSONObject("logic");
        UIModel uiModel = new UIModel();
        uiModel.setTitle(uiObj.getString("title"));
        uiModel.setBody(uiObj.getString("body"));
        uiModel.setPositiveButton(uiObj.getString("positive_button"));
        uiModel.setNegativeButton(uiObj.getString("negative_button"));
        rating.setUiModel(uiModel);
        rating.setSessionFirstInstallTime(logicObj.getLong("sessionFirstInstallTime"));
        rating.setSessionRepeatTime(logicObj.getLong("sessionRepeatTime"));
        rating.setSessionDifferenceTime(logicObj.getLong("sessionDifferenceTime"));
        rating.setRatingShowOnlyOnce(logicObj.getBoolean("ratingShowOnlyOnce"));
        rating.setRatingApi(logicObj.getBoolean("rating_api"));
        rating.setNegativeButtonPressedAndNotShowRating(logicObj.getBoolean("negativeButtonPressedAndNotShowRating"));
        return rating;
    }

    static Version parseUpdateJsonData(JSONObject updateObj) throws JSONException {
        Version version = new Version();
        JSONObject uiObj = updateObj.getJSONObject("ui");
        JSONObject logicObj = updateObj.getJSONObject("logic");
        UIModel uiModel = new UIModel();
        uiModel.setTitle(uiObj.getString("title"));
        uiModel.setBody(uiObj.getString("body"));
        uiModel.setPositiveButton(uiObj.getString("positive_button"));
        uiModel.setNegativeButton(uiObj.getString("negative_button"));
        version.setUiModel(uiModel);
        version.setMinimumVersion(logicObj.getInt("minimum_version"));
        version.setLatestVersionCode(logicObj.getInt("latest_version_code"));
        version.setNotificationType(logicObj.getString("notification_type"));
        return version;
    }

    /**
     * @apiNote throw new IllegalArgumentException();
     */
    public static void logIntegration(String tag, String message){
		Log.e(tag, ".     |  |");
		Log.e(tag, ".     |  |");
		Log.e(tag, ".     |  |");
		Log.e(tag, ".   \\ |  | /");
		Log.e(tag, ".    \\    /");
		Log.e(tag, ".     \\  /");
		Log.e(tag, ".      \\/");
		Log.e(tag, ".");
		Log.e(tag, message);
        Log.e(tag, ".");
        Log.e(tag, ".      /\\");
        Log.e(tag, ".     /  \\");
        Log.e(tag, ".    /    \\");
        Log.e(tag, ".   / |  | \\");
        Log.e(tag, ".     |  |");
        Log.e(tag, ".     |  |");
        Log.e(tag, ".");
    }
}
