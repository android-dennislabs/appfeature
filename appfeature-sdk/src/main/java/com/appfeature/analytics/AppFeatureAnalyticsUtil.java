package com.appfeature.analytics;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.BuildConfig;
import com.google.firebase.analytics.FirebaseAnalytics;

public class AppFeatureAnalyticsUtil {

    private final FirebaseAnalytics mFirebaseAnalytics;

    AppFeatureAnalyticsUtil(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }


    public void setUserProperty(String key, String value) {
        if (mFirebaseAnalytics != null) {
            mFirebaseAnalytics.setUserProperty(key, value);
            if (BuildConfig.DEBUG) {
                Log.d("@Analytics-user", key + ":" + value);
            }
        }
    }

    // custom parameter must be <= 24 characters
    // custom value must be <= 36 characters
    public void sendEvent(String eventName, Bundle params) {
        // custom event must be <= 32 characters
        if (mFirebaseAnalytics != null && params != null) {
            mFirebaseAnalytics.logEvent(eventName, params);
            if (BuildConfig.DEBUG) {
                for (String key : params.keySet()) {
                    Log.d("@Analytics-" + eventName, key + ":" + params.getString(key));
                }
            }
        }
    }
}
