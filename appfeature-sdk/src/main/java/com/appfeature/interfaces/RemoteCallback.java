package com.appfeature.interfaces;


import com.appfeature.utility.RemoteModel;

public interface RemoteCallback {

    void onComplete(String status, RemoteModel remoteData);
    void onError(String message);
}
