package com.appfeature.feature;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appfeature.R;
import com.appfeature.utility.DateUtil;
import com.appfeature.utility.ReviewApi;
import com.appfeature.utility.UIModel;
import com.appfeature.utility.UserPreference;
import com.appfeature.utility.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to display rating screen on given layout
 *
 * @author Abhijit Rao
 * @version 1.0
 * @since 2018.12.07
 */
public class RateUs {

    private static final long DEFAULT_FIRST_INSTALL_TIME = 48;//2days
    private static final long DEFAULT_REPEAT_TIME = 72;//3days
    private static final long DEFAULT_DIFFERENCE_TIME = 24;//1days
    private static final boolean DEFAULT_SINGLE_USE_ONLY = true;
    private static final boolean DEFAULT_RATING_API = true;
    private final Activity activity;
    private final ReviewApi reviewApi;
    private List<RelativeLayout> viewGroup = new ArrayList<>();
    private final String packageName;
    private final UserPreference userPref;

    private long sessionFirstInstallTime; //sessionFirstInstallTime
    private long sessionRepeatTime; //sessionRepeatTime
    private long sessionDifferenceTimeInHours;
    private boolean ratingShowOnlyOnce;
    private boolean ratingApi;

    //base functionality variables
    private String sessionInstallTime;
    private String sessionLastTime;
    private String sessionCurrentTime;
    private UIModel uiData;
    private boolean negativeButtonPressedAndNotShowRating;

    private RateUs(Activity activity) {
        this.activity = activity;
        this.packageName = activity.getApplicationContext().getPackageName();
        //default initialization
        this.sessionFirstInstallTime = DEFAULT_FIRST_INSTALL_TIME;
        this.ratingShowOnlyOnce = DEFAULT_SINGLE_USE_ONLY;
        this.ratingApi = DEFAULT_RATING_API;
        this.sessionRepeatTime = DEFAULT_REPEAT_TIME;
        this.sessionDifferenceTimeInHours = DEFAULT_DIFFERENCE_TIME;
        this.negativeButtonPressedAndNotShowRating = false;
        userPref = new UserPreference(activity, packageName);
        reviewApi = new ReviewApi(activity);
    }

    private static RateUs rateUs;

    public static RateUs getInstance() {
        return rateUs;
    }

    public static RateUs newInstance(Activity activity) {
        rateUs = new RateUs(activity);
        return rateUs;
    }

    public RateUs setSessionFirstInstallTime(long sessionFirstInstallTime) {
        this.sessionFirstInstallTime = sessionFirstInstallTime;
        return this;
    }

    public RateUs setSessionRepeatTime(long sessionRepeatTime) {
        this.sessionRepeatTime = sessionRepeatTime;
        return this;
    }

    public RateUs setSessionDifferenceTime(long sessionDifferenceTimeInHours) {
        this.sessionDifferenceTimeInHours = sessionDifferenceTimeInHours;
        return this;
    }

    public RateUs setRatingShowOnlyOnce(boolean ratingShowOnlyOnce) {
        this.ratingShowOnlyOnce = ratingShowOnlyOnce;
        return this;
    }

    public RateUs setRatingApi(boolean ratingApi) {
        this.ratingApi = ratingApi;
        return this;
    }

    public RateUs setNegativeButtonPressedAndNotShowRating(boolean negativeButtonPressedAndNotShowRating) {
        this.negativeButtonPressedAndNotShowRating = negativeButtonPressedAndNotShowRating;
        return this;
    }

    public RateUs setUiData(UIModel uiModel) {
        this.uiData = uiModel;
        return this;
    }

    public void init() {
        sessionInstallTime = userPref.getInstallationDate();
        sessionLastTime = userPref.getLastUsageDate();
        sessionCurrentTime = userPref.getCurrentDate();

        startDateValidationProcess();
    }

    private boolean isShowUI = false;
    private boolean isShowFirstTime = false;
    private boolean isRatingNegativeButtonPressedAndNotShow = false;

    public void showUI(RelativeLayout relativeLayout) {
        if (isShowUI && userPref != null && !userPref.isRatingSubmitted()) {
            if (isShowFirstTime) {
                if (!isRatingNegativeButtonPressedAndNotShow && !ratingShowOnlyOnce) {
                    if (relativeLayout != null) {
                        addScreenOnView(relativeLayout);
                    } else if (ratingApi) {
                        requestReviewFlow();
                    }
                }
            } else {
                isShowFirstTime = true;
                if (relativeLayout != null) {
                    addScreenOnView(relativeLayout);
                } else if (ratingApi) {
                    requestReviewFlow();
                }
            }
        }
    }

    private void requestReviewFlow() {
        if (reviewApi != null) {
            reviewApi.requestReviewFlowAPI();
        }
    }

    private void startDateValidationProcess() {
        if (userPref.isFirstInstallCheck()) {
            if (isFirstInstallCheckSession()) {
                userPref.setFirstInstallCheck(false);
                isShowUI = true;
            }
        } else {
            if (isUserActiveAndValidSession()) {
                isShowUI = true;
            }
        }

    }

    private boolean isFirstInstallCheckSession() {
        long diffBwCurrentAndInstall = DateUtil.differenceBetween(sessionCurrentTime, sessionInstallTime);
        if (diffBwCurrentAndInstall > sessionFirstInstallTime) {
            if (diffBwCurrentAndInstall < sessionRepeatTime) {
                return true;
            } else {
                updateLastUsageDate(true);
            }
        }
        return false;
    }

    private boolean isUserActiveAndValidSession() {
        long diffBwCurrentAndLast = DateUtil.differenceBetween(sessionCurrentTime, sessionLastTime);
        if (diffBwCurrentAndLast > sessionDifferenceTimeInHours) {
            if (diffBwCurrentAndLast < sessionRepeatTime) {
                return true;
            } else {
                updateLastUsageDate(false);
            }
        }
        return false;
    }


    private void updateLastUsageDate(boolean isReInitializeProcess) {
        //update last usage date
        userPref.updateLastUsageDate();
        if (isReInitializeProcess) {
            //reinitialize installation date for restarting process again
            userPref.reInitializeInstallationDate();
        }
    }

    private void addScreenOnView(RelativeLayout relativeLayout) {
        viewGroup.add(relativeLayout);
        LayoutInflater inflater = LayoutInflater.from(relativeLayout.getContext());
        relativeLayout.removeAllViews();
        setViewHolder(inflater.inflate(R.layout.rating_screen, relativeLayout));
    }

    private View setViewHolder(View view) {
        TextView tvTitle = view.findViewById(R.id.tv_rate_title);
        TextView tvMessage = view.findViewById(R.id.tv_message);
        Button negative = view.findViewById(R.id.btn_negative);
        Button positive = view.findViewById(R.id.btn_positive);
        if (uiData != null) {
            String title = uiData.getTitle();
            if (!title.equals("") && tvTitle != null) {
                tvTitle.setText(title);
            }
            String message = uiData.getBody();
            tvMessage.setText(message);
            positive.setText(uiData.getPositiveButton());
            negative.setText(uiData.getNegativeButton());
        }
        if (ratingShowOnlyOnce) {
            userPref.setRatingShowOnlyOnce(true);
        }
        updateLastUsageDate(true);
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanParentLayoutList();
                if (negativeButtonPressedAndNotShowRating) {
                    isRatingNegativeButtonPressedAndNotShow = true;
//                    userPref.setRatingNegativeButtonPressedAndNotShow(true);
                }
            }
        });
        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateUs(activity);
                cleanParentLayoutList();
                userPref.ratingSubmitted();
            }
        });
        return view;
    }

    private void cleanParentLayoutList() {
        for (RelativeLayout layout : viewGroup) {
            if (layout != null && layout.getChildCount() > 0) {
                layout.removeAllViews();
            }
        }
        viewGroup.clear();
    }

    private void rateUs(Activity activity) {
        Uri applicationUrl = Uri.parse(Utility.PLAY_STORE_URL + packageName);
        Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, applicationUrl);
        activity.startActivity(browserIntent1);
    }
}
