## Android Setup
 
Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven {
            url "https://jitpack.io"
        }
    }
}
```

Add this to your project build.gradle
#### Dependency
[![](https://jitpack.io/v/org.bitbucket.android-dennislabs/appfeature.svg)](https://jitpack.io/#org.bitbucket.android-dennislabs/appfeature)
```gradle
dependencies {
        implementation 'org.bitbucket.android-dennislabs:appfeature:2.1'
}
```

## Firebase Setup [Remote Config](https://console.firebase.google.com)
 
 
- **Parameter Key**

Note : Replace parameter key value "com_likeandupdate" with your own app package name 
```java
    com_likeandupdate_appsfeature

```

- **Default value**
```json
{
  "rating": {
    "ui": {
      "positive_button": "Rate Us",
      "negative_button": "Remind me later",
      "title": "Rating",
      "body": "If you enjoy this app, please take a moment to rate this app"
    },
    "logic": {
      "sessionFirstInstallTime": 48,
      "sessionRepeatTime": 72,
      "sessionDifferenceTime": 24,
      "ratingShowOnlyOnce": false,
      "negativeButtonPressedAndNotShowRating": false
    }
  },
  "app_update": {
    "ui": {
      "positive_button": "Download",
      "negative_button": "Cancel",
      "title": "Update",
      "body": "There is newer version of this application available, Click Download to redirect google play store."
    },
    "logic": {
      "minimum_version": 1,
      "latest_version_code": 1,
      "notification_type": "ONCE"
    }
  }
}
```

## Java usage code

```java
public class AppApplication extends Application {

    private static AppApplication appApplication;


    public static AppApplication getInstance() {
        return appApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appApplication = this;
        ActivityLifecycleObserver.getInstance().register(this);
    }
    
    private AppsFeature appsFeature;
    
    public void initAppFeature(Activity activity) {
        if (appsFeature == null) {
            appsFeature = AppsFeature.getInstance(activity, BuildConfig.VERSION_CODE);
            if (activity instanceof VersionCallback) {
                appsFeature.addVersionCallback((VersionCallback) activity);
            }
            appsFeature.init();
        }
    }  

    public void showRating(RelativeLayout layout){
        if(appsFeature!=null){
            appsFeature.showRating(layout);
        }
    }
}//

```

```java
public abstract class AppFeatureBaseActivity extends AppCompatActivity implements VersionCallback{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppApplication.getInstance().initAppFeature(this);
    }

    @Override
    public void showVersionDialog(Boolean restrictToUpdate, UIModel uiData) {
        onVersionUpdate();
    }

    protected abstract void onVersionUpdate();

} 

```

Basic methods of AppFeature are :

```java
    private void setRating(Activity activity) {
        RelativeLayout container = findViewById(R.id.rating_container);
        AppApplication.getInstance().showRating(container) 
    } 

```